## Setup
  1. Run through docker compose, e.g.:
        ```
        $ docker-compose up -d --build
        ```
  2. Apply migrations:
        ```
        $ docker-compose exec web python manage.py migrate
        ```
  3. Load example data (order important):
        ```
        $ docker-compose exec web python manage.py loaddata test_data/dishes.json
        ```
        ```
        $ docker-compose exec web python manage.py loaddata test_data/cards.json
        ```
  4. Create user (for testing purposes), e.g.:
        ```
        $ docker-compose exec web python manage.py createsuperuser
        ```
     
## Unit Tests
  Run unit tests with
        ```
        $ docker-compose exec web python manage.py test
        ```

## Local environment
  Available under `http://localhost:8000`

## Swagger
  Available under `http://localhost:8000/api/schema/swagger-ui`
