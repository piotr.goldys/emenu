from django.core.validators import MinValueValidator
from django.db import models

from django.db.models import (
    Model,
    DateTimeField,
    CharField,
    TextField,
    ManyToManyField,
    PositiveIntegerField,
    BooleanField,
    DecimalField,
)
from django.utils import timezone


class TimestampModel(Model):
    created_at = DateTimeField(default=timezone.now)
    updated_at = DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Dish(TimestampModel):
    name = CharField(max_length=128, db_index=True)
    description = TextField()

    # We don't store currency, assuming single currency is used within the system.
    # In case we want to handle multiple, we can add separate field or use `django-money` for example.
    price = DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0)])

    preparation_time = PositiveIntegerField()  # In minutes
    is_vege = BooleanField(default=False)
    image = models.ImageField(upload_to='images/', blank=True, null=True)

    def __str__(self):
        return self.name


class Card(TimestampModel):
    name = CharField(max_length=128, unique=True, db_index=True)
    description = TextField()
    dishes = ManyToManyField(Dish, blank=True, related_name='dishes')

    def __str__(self):
        return self.name
