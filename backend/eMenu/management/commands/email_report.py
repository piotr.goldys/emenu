from datetime import date, timedelta

from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.core.management import BaseCommand
from django.template.loader import render_to_string

from backend.settings import EMAIL_HOST_USER
from eMenu.models import Dish


class Command(BaseCommand):
    help = 'Send reports with dish updates.'

    def handle(self, *args, **options):
        today = date.today()
        yesterday = today - timedelta(days=1)

        dishes_created_yesterday = Dish.objects.filter(created_at__date=yesterday)
        dishes_updated_yesterday = Dish.objects.filter(updated_at__date=yesterday)

        html_context = {
            'dishes_created': dishes_created_yesterday or {},
            'dishes_updated': dishes_updated_yesterday or {},
        }

        subject_email = f'API eMenu - Database Update - {yesterday.strftime("%d-%m-%Y")}'
        html_body = render_to_string('eMenu/email.html', html_context)

        addressees = [user.email for user in User.objects.all()]

        email = EmailMessage(subject=subject_email, body=html_body, from_email=EMAIL_HOST_USER, to=addressees)
        email.content_subtype = 'html'
        email.send()
        self.stdout.write('E-mail reports have been successfully sent.')
