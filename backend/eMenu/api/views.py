from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet

from eMenu.api.filters import CardOrderingFilter, CardFilter, DishFilter
from eMenu.api.serializers import CardSerializer, DishSerializer
from eMenu.models import Card, Dish


class CardViewSet(ModelViewSet):
    serializer_class = CardSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    filter_backends = (
        DjangoFilterBackend,
        CardOrderingFilter,
    )
    filterset_class = CardFilter
    ordering_fields = (
        'name',
        'dishes_count',
    )

    def get_queryset(self):
        queryset = Card.objects.all()

        # Show cards without dishes only for authenticated users.
        if not self.request.user.is_authenticated:
            queryset = queryset.exclude(dishes__isnull=True)

        return queryset


class DishViewSet(ModelViewSet):
    queryset = Dish.objects.all()
    serializer_class = DishSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    filter_backends = (
        DjangoFilterBackend,
        OrderingFilter,
    )
    filterset_class = DishFilter
    ordering_fields = (
        'name',
        'price',
        'preparation_time',
    )
    parser_classes = (MultiPartParser, FormParser)  # Allow image uploads
