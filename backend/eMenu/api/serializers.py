from rest_framework.serializers import ModelSerializer

from eMenu.models import Card, Dish


class DishSerializer(ModelSerializer):
    class Meta:
        model = Dish
        fields = (
            'pk',
            'name',
            'description',
            'price',
            'preparation_time',
            'is_vege',
            'created_at',
            'updated_at',
            'image',
        )
        extra_kwargs = {
            'created_at': {'required': False},
            'updated_at': {'required': False},
            'image': {'required': False},
        }


class CardSerializer(ModelSerializer):
    class Meta:
        model = Card
        fields = (
            'pk',
            'name',
            'description',
            'created_at',
            'updated_at',
            'dishes',
        )
        extra_kwargs = {
            'created_at': {'required': False},
            'updated_at': {'required': False},
        }

    # Provide convenient way to add dishes when creating Card from DRF browsable API
    def to_representation(self, instance):
        card = super().to_representation(instance)
        card['dishes'] = DishSerializer(instance.dishes.all(), many=True).data
        return card
