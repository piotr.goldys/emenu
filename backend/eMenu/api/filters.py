from django.db.models import Count
from django_filters import rest_framework as filters
from django_filters.rest_framework import FilterSet
from rest_framework.filters import OrderingFilter

from eMenu.models import Card, Dish


class CardOrderingFilter(OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)

        if ordering:
            queryset = queryset.annotate(dishes_count=Count('dishes'))
            return queryset.order_by(*ordering)

        return queryset


class CardFilter(FilterSet):
    start_created_at = filters.DateTimeFilter(
        field_name='created_at', lookup_expr='date__gte', label='Start Created Date'
    )
    end_created_at = filters.DateTimeFilter(
        field_name='created_at', lookup_expr='date__lte', label='End Created Date'
    )
    start_updated_at = filters.DateTimeFilter(
        field_name='updated_at', lookup_expr='date__gte', label='Start Updated Date'
    )
    end_updated_at = filters.DateTimeFilter(
        field_name='updated_at', lookup_expr='date__lte', label='End Updated Date'
    )

    class Meta:
        model = Card
        fields = (
            'name',
            'start_created_at',
            'end_created_at',
            'start_updated_at',
            'end_updated_at',
        )


class DishFilter(FilterSet):
    start_created_at = filters.DateTimeFilter(
        field_name='created_at', lookup_expr='date__gte', label='Start Created Date'
    )
    end_created_at = filters.DateTimeFilter(
        field_name='created_at', lookup_expr='date__lte', label='End Created Date'
    )
    start_updated_at = filters.DateTimeFilter(
        field_name='updated_at', lookup_expr='date__gte', label='Start Updated Date'
    )
    end_updated_at = filters.DateTimeFilter(
        field_name='updated_at', lookup_expr='date__lte', label='End Updated Date'
    )

    class Meta:
        model = Dish
        fields = (
            'name',
            'start_created_at',
            'end_created_at',
            'start_updated_at',
            'end_updated_at',
        )
