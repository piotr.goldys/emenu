from decimal import Decimal

from django.contrib.auth import get_user_model
from django.core import mail
from django.core.management import call_command
from django.test import TestCase
from rest_framework.status import (
    HTTP_403_FORBIDDEN,
    HTTP_200_OK,
    HTTP_404_NOT_FOUND,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
)
from rest_framework.test import APITestCase

from eMenu.models import Dish, Card
from django.test.client import Client

DISHES_URL = '/dishes/'
CARDS_URL = '/cards/'
DISH_DATA = {
    'name': 'Test dish name',
    'description': 'Test dish description',
    'price': '14.99',
    'preparation_time': '20',
}
DISH_DATA_2 = {
    'name': 'Test dish name 2',
    'description': 'Test dish description 2',
    'price': '19.99',
    'preparation_time': '25',
}
CARD_DATA = {
    'name': 'Test card name',
    'description': 'Test card description',
}
CARD_DATA_2 = {
    'name': 'Test card name 2',
    'description': 'Test card description 2',
}
CARD_DATA_WITH_DISH = {**CARD_DATA, 'dishes': [DISH_DATA]}


class AuthenticatedAPITestCase(APITestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='TestUsername', password='TestPassword', email='TestEmail@test.com'
        )
        # Stay authenticated
        self.client.force_authenticate(user=self.user)


class DishesTestCase(APITestCase):
    @property
    def dish_url(self) -> str:
        return f'{DISHES_URL}{self.dish.pk}/'

    def setUp(self):
        super().setUp()
        self.dish = Dish.objects.create(**DISH_DATA)


class CardsTestCase(APITestCase):
    @property
    def card_url(self) -> str:
        return f'{CARDS_URL}{self.card.pk}/'

    def setUp(self):
        super().setUp()
        self.card = Card.objects.create(**CARD_DATA)


def test_list_ordering(client: Client, url: str, ordering: str):
    response = client.get(url, {'ordering': ordering})
    assert response.status_code == HTTP_200_OK

    results = response.data

    if ordering == 'price':
        assert list(results) == sorted(results, key=lambda d: Decimal(d[ordering]))
    else:
        assert list(results) == sorted(results, key=lambda d: d[ordering])


class TestDishesPublicAPI(DishesTestCase):
    def test_list(self):
        response = self.client.get(DISHES_URL)
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data[0].get('name'), DISH_DATA['name'])

    def test_detail(self):
        url = f'{DISHES_URL}{self.dish.pk}/'
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data.get('name'), DISH_DATA['name'])


class TestDishesPublicAPIOrderingAndFilters(DishesTestCase):
    def setUp(self):
        super().setUp()
        call_command('loaddata', 'test_data/dishes.json')

    def test_ordering_by_name(self):
        test_list_ordering(self.client, DISHES_URL, 'name')

    def test_ordering_by_price(self):
        test_list_ordering(self.client, DISHES_URL, 'price')

    def test_ordering_by_preparation_time(self):
        test_list_ordering(self.client, DISHES_URL, 'preparation_time')


class TestCardsPublicAPI(CardsTestCase):
    def test_list(self):
        dish = Dish.objects.create(**DISH_DATA)
        self.card.dishes.add(dish)

        response = self.client.get(CARDS_URL)

        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data[0].get('name'), CARD_DATA['name'])
        self.assertEqual(response.data[0].get('dishes')[0]['name'], DISH_DATA['name'])

    def test_list_show_only_cards_with_dishes(self):
        response = self.client.get(CARDS_URL)
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data, [])

    def test_detail(self):
        dish = Dish.objects.create(**DISH_DATA)
        self.card.dishes.add(dish)

        response = self.client.get(f'{CARDS_URL}{self.card.pk}/')

        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response.data.get('name'), CARD_DATA['name'])
        self.assertEqual(response.data.get('dishes')[0]['name'], DISH_DATA['name'])

    def test_detail_show_only_cards_with_dishes(self):
        response = self.client.get(f'{CARDS_URL}{self.card.pk}/')

        # This card can be shown only after authentication.
        self.assertEqual(response.status_code, HTTP_404_NOT_FOUND)


class TestCardsPublicAPIOrderingAndFilters(CardsTestCase):
    def setUp(self):
        super().setUp()
        call_command('loaddata', 'test_data/dishes.json')
        call_command('loaddata', 'test_data/cards.json')

    def test_ordering_by_name(self):
        test_list_ordering(self.client, CARDS_URL, 'name')

    def test_filter_by_name(self):
        response = self.client.get(CARDS_URL, {'name': 'Hutong'})
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['name'], 'Hutong')

    def test_filter_by_start_created_at(self):
        response = self.client.get(CARDS_URL, {'start_created_at': '2021-01-01'})
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(len(response.data), 8)

    def test_filter_by_end_created_at(self):
        response = self.client.get(CARDS_URL, {'end_created_at': '2021-01-31'})
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_filter_by_start_and_end_created_at(self):
        response = self.client.get(
            CARDS_URL, {'start_created_at': '2021-02-01', 'end_created_at': '2021-02-03'}
        )
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(len(response.data), 6)


class TestDishesPrivateAPI(DishesTestCase, AuthenticatedAPITestCase):
    def test_create(self):
        response = self.client.post(DISHES_URL, DISH_DATA)
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertTrue(response.data.get('pk'))

    def test_update(self):
        response = self.client.patch(self.dish_url, DISH_DATA_2)
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.dish.refresh_from_db()
        self.assertEqual(self.dish.name, DISH_DATA_2['name'])

    def test_partial_update(self):
        response = self.client.patch(self.dish_url, {'name': 'new name'})
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.dish.refresh_from_db()
        self.assertEqual(self.dish.name, 'new name')

    def test_delete(self):
        response = self.client.delete(self.dish_url)
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
        self.assertRaises(Dish.DoesNotExist, self.dish.refresh_from_db)


class TestCardsPrivateAPI(CardsTestCase, AuthenticatedAPITestCase):
    def test_create(self):
        dish = Dish.objects.create(**DISH_DATA)
        card_data = {**CARD_DATA_2, 'dishes': [dish.pk]}

        response = self.client.post(CARDS_URL, card_data)
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertTrue(response.data.get('pk'))

    def test_update(self):
        response = self.client.patch(self.card_url, CARD_DATA_2)
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.card.refresh_from_db()
        self.assertEqual(self.card.name, CARD_DATA_2['name'])

    def test_partial_update(self):
        response = self.client.patch(self.card_url, {'name': 'new name'})
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.card.refresh_from_db()
        self.assertEqual(self.card.name, 'new name')

    def test_delete(self):
        response = self.client.delete(self.card_url)
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
        self.assertRaises(Card.DoesNotExist, self.card.refresh_from_db)


class TestDishesPrivateAPIAuth(DishesTestCase):
    def test_create_403(self):
        response = self.client.post(DISHES_URL, DISH_DATA)
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)

    def test_update_403(self):
        response = self.client.put(self.dish_url, DISH_DATA_2)
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)

    def test_partial_update_403(self):
        response = self.client.patch(self.dish_url, {'name': 'new name'})
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)

    def test_delete_403(self):
        response = self.client.delete(self.dish_url)
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)


class TestCardsPrivateAPIAuth(CardsTestCase):
    def test_create_403(self):
        response = self.client.post(CARDS_URL, CARD_DATA)
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)

    def test_update_403(self):
        response = self.client.put(self.card_url, CARD_DATA_2)
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)

    def test_partial_update_403(self):
        response = self.client.patch(self.card_url, {'name': 'new name'})
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)

    def test_delete_403(self):
        response = self.client.delete(self.card_url)
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)


class TestEmailReports(TestCase):
    def test_email_reports(self):
        get_user_model().objects.create_user(username='test_emails1', email='testemails1@gmail.com')
        get_user_model().objects.create_user(username='test_emails2', email='testemails2@gmail.com')

        call_command('email_report')

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(len(mail.outbox[0].recipients()), 2)
        self.assertIn('API eMenu - Database Update', mail.outbox[0].subject)
        self.assertIn("That's what happened yesterday", mail.outbox[0].body)
